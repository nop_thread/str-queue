//! A queue for a string.
//!
//! # Summary
//!
//! [`StrQueue`] is a queue for UTF-8 sequences.
//! Strings can be pushed into `StrQueue`, characters can be popped from the
//! queue, and the queue can be printed or converted into a string.
//!
//! # Details
//!
//! ## Non-contiguous
//!
//! `StrQueue` does not guarantee that the content is stored on a contiguous
//! memory region. As of writing this document, `StrQueue` internally uses a
//! ring buffer.
//!
//! ## Push
//!
//! `StrQueue` accepts not only strings to be pushed, but also arbitrary bytes.
//! When bytes (including valid UTF-8 strings) are pushed, the things below
//! (theoretically) happen in order:
//!
//! 1.  Bytes are appended to the internal buffer.
//! 2.  Internal buffers are validated.
//!       * If the buffer has incomplete bytes at the end of the buffer,
//!         they are preserved (at least the until the next bytes are added).
//!           * "Incomplete bytes" here is byte sequence which can be valid
//!             UTF-8 sequence if appropriate bytes are appended.
//!       * Other ill-formed bytes than that are replaced with
//!         [`U+FFFD REPLACEMENT CHARACTER`][`core::char::REPLACEMENT_CHARACTER`]
//!         using the same rule as
//!         [`String::from_utf8_lossy`][`alloc::string::String::from_utf8_lossy`].
//!
//! ## Pop
//!
//! Pop operations are basically performed to characters, rather than raw bytes.
//! For example, [`StrQueue::pop_char`] pops the first character, not the first byte.
//! This principle helps the queue to keep the internal bytes valid as UTF-8 string.
//!
//! ## Conversion
//!
//! When converting a queue into a string, caller can choose how to handle the
//! possible trailing incomplete bytes.
//! [`PartialHandling::Ignore`] lets methods ignore such incomplete bytes (if
//! exist), and [`PartialHandling::Emit`] lets methods emit them as a `U+FFFD
//! REPLACEMENT CHARACTER`.
//!
//! ## Range
//!
//! This crate provides adaptors for subrange access: [`BytesRange`] and
//! [`CharsRange`]. Subrange adaptor types plays a similar role as [`&str`] for
//! [`String`]: they are views to the subrange of the underlying `StrQueue`.
//!
//! [`BytesRange`] can point to arbitrary subrange.
//! [`CharsRange`] can only point to a subrange that are valid UTF-8 string and
//! the last possibly incomplete character (i.e. the same condition as
//! [`StrQueue`].
//!
//! # Usage
//!
//! ## Creation
//!
//! Default queue created by [`StrQueue::new`] is empty.
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::new();
//! assert!(queue.display(PartialHandling::Emit).to_string().is_empty());
//! ```
//!
//! A queue can be created with initial contents by [`StrQueue::from`].
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::from("hello");
//! assert_eq!(queue.display(PartialHandling::Emit).to_string(), "hello");
//! ```
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::from(b"he\xf0llo\xce");
//! assert_eq!(queue.display(PartialHandling::Emit).to_string(), "he\u{FFFD}llo\u{FFFD}");
//! ```
//!
//! ## Push
//!
//! Bytes and string can be pushed.
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let mut queue = StrQueue::new();
//!
//! queue.push_str("hello");
//! assert_eq!(queue.display(PartialHandling::Emit).to_string(), "hello");
//!
//! queue.push_bytes(b" world");
//! assert_eq!(queue.display(PartialHandling::Emit).to_string(), "hello world");
//! ```
//!
//! ## Pop
//!
//! The first characters can be popped using [`StrQueue::pop_char`].
//!
//! ```
//! use str_queue::StrQueue;
//!
//! let mut queue = StrQueue::from(b"ab\xf0");
//! assert_eq!(queue.pop_char(), Some('a'));
//! assert_eq!(queue.pop_char(), Some('b'));
//! // Incomplete bytes are simply ignored as they are not a character.
//! assert_eq!(queue.pop_char(), None);
//! ```
//!
//! The first line can be popped using [`StrQueue::pop_line`].
//!
//! ```
//! use str_queue::StrQueue;
//!
//! // Note that the last "world\r" is not considered as a complete line
//! // since the line can be finally "world\r" or "world\r\n".
//! let mut queue = StrQueue::from("Hello\nworld\r\nGoodbye\rworld\r");
//!
//! // The popped line can be dropped.
//! queue.pop_line();
//! // The popped line can be accessed.
//! assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), Some("world\r\n"));
//! assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), Some("Goodbye\r"));
//! assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), None);
//! assert_eq!(queue.chars_range(..), "world\r");
//! ```
//!
//! ## Ranges
//!
//! Bytes ranges can be created by [`StrQueue::bytes_range`] method.
//!
//! ```
//! use str_queue::StrQueue;
//!
//! let queue = StrQueue::from("alpha\u{03B1}beta\u{03B2}");
//! let bytes = queue.bytes_range(6..12);
//! assert_eq!(bytes, b"\xb1beta\xce"[..]);
//! ```
//!
//! Chars ranges can be created by [`StrQueue::chars_range`] method.
//! `CharsRange` implements [`Display`][`core::fmt::Display`] and
//! [`ToString`][`alloc::string::ToString`] traits, since the it is guaranteed
//! to have a valid string as a prefix (with possibly incomplete character as
//! a suffix).
//!
//! ```
//! use str_queue::StrQueue;
//!
//! let queue = StrQueue::from("alpha\u{03B1}beta\u{03B2}");
//! let range = queue.chars_range(7..);
//! assert_eq!(range, "beta\u{03B2}");
//! assert_eq!(range.to_string(), "beta\u{03B2}");
//! ```
//!
//! Subranges can be created from ranges.
//!
//! ```
//! use str_queue::StrQueue;
//!
//! let queue = StrQueue::from("Hello world");
//! let bytes = queue.bytes_range(6..);
//! assert_eq!(bytes, b"world"[..]);
//!
//! let subrange = bytes.range(1..4);
//! assert_eq!(subrange, b"orl"[..]);
//! ```
//!
//! ```
//! use str_queue::StrQueue;
//!
//! let queue = StrQueue::from("Hello world");
//! let range = queue.chars_range(6..);
//! assert_eq!(range, "world");
//!
//! let subrange = range.range(1..4);
//! assert_eq!(subrange, "orl");
//! ```
//!
//! ## Conversion
//!
//! Note that `StrQueue` does not guarantee the content is stored on a
//! contiguous memory region.
//! So, you cannot get the content string as a single `&str` without paying
//! additional cost.
//!
//! ### Display
//!
//! [`StrQueue::display`] returns a helper struct which implements
//! [`core::fmt::Display`]. It can be written to a formatter or converted to
//! a string using `.to_string()`.
//!
//! To create [`String`][`alloc::string::String`], use `.to_string()`.
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::from("hello");
//!
//! let s: String = queue.display(PartialHandling::Emit).to_string();
//! assert_eq!(s, "hello");
//! ```
//!
//! To write the string into a writer, use the return value directly.
//!
//! ```
//! use core::fmt::Write as _;
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::from("hello");
//! // `String` implements `core::fmt::Write`, and can be used for `write!()`.
//! let mut buf = String::new();
//!
//! write!(buf, "{}", queue.display(PartialHandling::Emit));
//! assert_eq!(buf, "hello");
//! ```
//!
//! ### Fragments
//!
//! [`StrQueue::fragments`] returns an iterator of [`Fragment`]s, which can hold
//! a substring slices and a characters.
//!
//! ```
//! use str_queue::{Fragment, PartialHandling, StrQueue};
//!
//! // Note that `\xce` can appear as a first byte of valid UTF-8 sequence.
//! let queue = StrQueue::from(b"hello \xce");
//!
//! let mut buf = String::new();
//! for frag in queue.fragments(PartialHandling::Emit) {
//!     match frag {
//!         Fragment::Str(s) => buf.push_str(s),
//!         Fragment::Char(c) => buf.push(c),
//!         Fragment::Incomplete => buf.push_str("<<incomplete>>"),
//!     }
//! }
//! assert_eq!(buf, "hello <<incomplete>>");
//! ```
//!
//! ### Characters
//!
//! [`StrQueue::chars`] and [`StrQueue::into_chars`] return an iterator of [`char`]s.
//!
//! `StrQueue::chars` does not consume the queue.
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::from("hello");
//! let mut chars = queue.chars(PartialHandling::Emit);
//! assert_eq!(chars.next(), Some('h'));
//! assert_eq!(chars.next(), Some('e'));
//! assert_eq!(chars.next(), Some('l'));
//! assert_eq!(chars.next(), Some('l'));
//! assert_eq!(chars.next(), Some('o'));
//! assert_eq!(chars.next(), None);
//! // `StrQueue::chars()` does not consume the queue, so it can be used here.
//! assert_eq!(
//!     queue.chars(PartialHandling::Emit).collect::<String>(),
//!     "hello"
//! );
//! ```
//!
//! `StrQueue::into_chars` consumes the queue.
//!
//! ```
//! use str_queue::{PartialHandling, StrQueue};
//!
//! let queue = StrQueue::from("hello");
//! let mut chars = queue.into_chars(PartialHandling::Emit);
//! assert_eq!(chars.next(), Some('h'));
//! assert_eq!(chars.next(), Some('e'));
//! assert_eq!(chars.next(), Some('l'));
//! assert_eq!(chars.next(), Some('l'));
//! assert_eq!(chars.next(), Some('o'));
//! assert_eq!(chars.next(), None);
//! // `queue` is no longer available, as it is consumeb dy `queue.into_chars()`.
//! ```
#![cfg_attr(not(feature = "std"), no_std)]
#![forbid(unsafe_code)]
#![warn(rust_2018_idioms)]
// `clippy::missing_docs_in_private_items` implies `missing_docs`.
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::unwrap_used)]

extern crate alloc;

mod iter;
mod range;
mod utf8;

use core::cmp::Ordering;
use core::fmt;
use core::hash;
use core::mem;
use core::num::NonZeroUsize;
use core::ops::RangeBounds;
use core::str;

use alloc::collections::VecDeque;

pub use crate::iter::{Chars, Fragment, Fragments, IntoChars};
pub use crate::range::{BytesRange, CharsRange, CharsRangePoppedLine};
use crate::utf8::REPLACEMENT_CHAR_STR;

/// Queue for a string.
///
/// This queue can contain incomplete UTF-8 byte sequences at the tail.
/// However, if it became sure that the sequence is invalid as UTF-8 sequence,
/// the invalid bytes would be replaced by U+FFFD REPLACEMENT CHARACTER.
#[derive(Default, Debug, Clone)]
pub struct StrQueue {
    /// Inner deque.
    inner: VecDeque<u8>,
    /// Length of the trailing bytes that are possibly invalid UTF-8 sequence.
    ///
    /// This must always be less than 4.
    len_incomplete: u8,
}

/// `StrQueue` creation.
impl StrQueue {
    /// Creates an empty `StrQueue`.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let deque = StrQueue::new();
    /// ```
    #[inline]
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates an empty `StrQueue` with at least the given capacity.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let deque = StrQueue::with_capacity(42);
    ///
    /// assert!(deque.capacity() >= 42);
    /// ```
    #[inline]
    #[must_use]
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            inner: VecDeque::with_capacity(capacity),
            len_incomplete: 0,
        }
    }
}

/// Capacity.
impl StrQueue {
    /// Returns the number of bytes the `StrQueue` can hold without reallocating.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let deque = StrQueue::with_capacity(42);
    ///
    /// assert!(deque.capacity() >= 42);
    /// ```
    #[inline]
    #[must_use]
    pub fn capacity(&self) -> usize {
        self.inner.capacity()
    }

    /// Reserves capacity for at least `additional` more bytes.
    #[inline]
    pub fn reserve(&mut self, additional: usize) {
        self.inner.reserve(additional);
    }

    /// Reserves capacity for exact `additional` more bytes.
    #[inline]
    pub fn reserve_exact(&mut self, additional: usize) {
        self.inner.reserve_exact(additional);
    }

    /// Shrinks the capacity of the `StrQueue` as much as possible.
    #[inline]
    pub fn shrink_to_fit(&mut self) {
        self.inner.shrink_to_fit();
    }
}

/// Subrange access.
impl StrQueue {
    /// Returns the subrange accessor for the queue.
    #[inline]
    #[must_use]
    pub fn bytes_range<R>(&self, range: R) -> BytesRange<'_>
    where
        R: RangeBounds<usize>,
    {
        BytesRange::new(self, range)
    }

    /// Returns the subrange accessor for the queue.
    ///
    /// The returned range can contain an incomplete character at the end.
    /// If you want to exclude a possible trailing incomplete character in the range,
    /// use [`CharsRange::to_complete`] or [`CharsRange::trim_last_incomplete_char`].
    ///
    /// # Panics
    ///
    /// Panics if the start bound of the range does not lie on UTF-8 sequence boundary.
    #[inline]
    #[must_use]
    pub fn chars_range<R>(&self, range: R) -> CharsRange<'_>
    where
        R: RangeBounds<usize>,
    {
        CharsRange::new(self, range)
    }
}

/// Content length and existence.
impl StrQueue {
    /// Returns the string length in bytes, including incomplete bytes.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let queue = StrQueue::from(b"hello\xce");
    /// assert_eq!(queue.len(), 6);
    /// ```
    #[inline]
    #[must_use]
    pub fn len(&self) -> usize {
        self.inner.len()
    }

    /// Returns true if the queue is completely empty (i.e. has no bytes).
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::new();
    /// assert!(queue.is_empty());
    ///
    /// queue.push_str("hello");
    /// assert!(!queue.is_empty());
    /// ```
    #[inline]
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.inner.is_empty()
    }

    /// Returns the string length in bytes, excluding incomplete bytes.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let queue = StrQueue::from(b"hello\xce");
    /// assert_eq!(queue.len_complete(), 5);
    /// ```
    #[inline]
    #[must_use]
    pub fn len_complete(&self) -> usize {
        self.inner.len() - self.len_incomplete()
    }

    /// Returns the length of incomplete bytes.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let queue = StrQueue::from(b"hello\xce");
    /// assert_eq!(queue.len_incomplete(), 1);
    /// ```
    #[inline]
    #[must_use]
    pub fn len_incomplete(&self) -> usize {
        usize::from(self.len_incomplete)
    }

    /// Returns true if the `StrQueue` contains no complete string.
    ///
    /// This returns the same value as `self.first().is_none()`.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::new();
    /// assert!(queue.is_empty_complete());
    ///
    /// queue.push_bytes(b"\xce");
    /// assert!(queue.is_empty_complete());
    /// assert_eq!(queue.first_char(), None);
    ///
    /// queue.push_bytes(b"\xb1");
    /// assert!(!queue.is_empty_complete());
    /// assert_eq!(queue.first_char(), Some('\u{03B1}'));
    /// ```
    #[inline]
    #[must_use]
    pub fn is_empty_complete(&self) -> bool {
        self.inner.len() == self.len_incomplete as usize
    }

    /// Returns true if the content is a complete string, i.e. has no trailing
    /// incomplete character.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::from(b"abc\xce");
    /// assert!(!queue.is_complete());
    /// queue.push_bytes(b"\xb1");
    /// // Now the string is "abc\u{03B1}".
    /// assert!(queue.is_complete());
    /// ```
    pub fn is_complete(&self) -> bool {
        self.len_incomplete == 0
    }
}

/// Buffer and content manipulation.
impl StrQueue {
    /// Clears the `StrQueue`, removing all bytes.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from(b"Hello \xce");
    /// assert!(!queue.is_empty());
    ///
    /// queue.clear();
    /// assert!(queue.is_empty());
    /// ```
    #[inline]
    pub fn clear(&mut self) {
        self.inner.clear();
    }

    /// Trims the last incomplete character, and returns the length of the trimmed bytes.
    ///
    /// If the string is complete (i.e. no incomplete character follows), does
    /// nothing and returns 0.
    ///
    /// If you want to get a modified copy instead of modifying `self` itself,
    /// use [`CharsRange::to_complete`].
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from(b"Hello \xce");
    /// assert_eq!(
    ///     queue.display(PartialHandling::Emit).to_string(),
    ///     "Hello \u{FFFD}"
    /// );
    ///
    /// queue.trim_last_incomplete_char();
    /// assert_eq!(queue.display(PartialHandling::Emit).to_string(), "Hello ");
    /// ```
    pub fn trim_last_incomplete_char(&mut self) -> usize {
        self.inner.truncate(self.len_complete());
        usize::from(mem::replace(&mut self.len_incomplete, 0))
    }

    /// Pushes the given string to the queue.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from("hello");
    /// queue.push_str(" world");
    ///
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "hello world"
    /// );
    /// ```
    pub fn push_str(&mut self, s: &str) {
        if self.len_incomplete != 0 {
            self.inner.truncate(self.len_complete());
            self.inner.extend(REPLACEMENT_CHAR_STR.as_bytes());
            self.len_incomplete = 0;
        }
        self.inner.extend(s.as_bytes());
    }

    /// Pushes the given character to the queue.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from("hello");
    /// queue.push_char('!');
    ///
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "hello!"
    /// );
    /// ```
    pub fn push_char(&mut self, c: char) {
        let mut buf = [0_u8; 4];
        self.push_str(c.encode_utf8(&mut buf));
    }

    /// Pushes the given bytes to the queue.
    ///
    /// Invalid UTF-8 sequences not at the last would be replaced with
    /// `U+FFFD REPLACEMENT CHARACTER`.
    /// The last incomplete sequences would be kept, since it might become valid
    /// when more bytes are appended later.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from(b"alpha \xce");
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha \u{FFFD}"
    /// );
    ///
    /// queue.push_bytes(b"\xb1");
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha \u{03B1}"
    /// );
    /// ```
    pub fn push_bytes(&mut self, mut bytes: &[u8]) {
        // Process incomplete bytes.
        loop {
            let (rest, has_progress) = self.process_incomplete_bytes(bytes);
            if !has_progress {
                break;
            }
            bytes = rest;
        }
        if self.len_incomplete != 0 {
            assert!(
                bytes.is_empty(),
                "[consistency] it is not expected to give up processing \
                 incomplete bytes before consuming all the inputs"
            );
            return;
        }

        while !bytes.is_empty() {
            let e = match str::from_utf8(bytes) {
                Ok(s) => {
                    self.inner.extend(s.as_bytes());
                    return;
                }
                Err(e) => e,
            };
            let valid_up_to = e.valid_up_to();
            if let Some(error_len) = e.error_len() {
                // Replace invalid bytes with U+FFFD, and resume.
                self.inner.extend(&bytes[..valid_up_to]);
                self.inner.extend(REPLACEMENT_CHAR_STR.as_bytes());
                bytes = &bytes[(valid_up_to + error_len)..];
                continue;
            } else {
                self.inner.extend(bytes);
                self.len_incomplete = (bytes.len() - valid_up_to) as u8;
                return;
            }
        }
    }

    /// Processes incomplete bytes and returns a pair of the rest input and the progress.
    ///
    /// If there are progress, `(_, true)` is returned.
    /// If no progress can be made, `(_, false)` is returned.
    fn process_incomplete_bytes<'a>(&mut self, bytes: &'a [u8]) -> (&'a [u8], bool) {
        let len_incomplete = usize::from(self.len_incomplete);
        if len_incomplete == 0 {
            // Nothing to process.
            return (bytes, false);
        }

        // Load already-known incomplete bytes.
        let mut buf = [0_u8; 4];
        self.inner
            .range(self.len_complete()..)
            .zip(&mut buf[..])
            .for_each(|(src, dest)| *dest = *src);
        let len_expected = usize::from(utf8::expected_char_len(buf[0]));

        let len_from_input = len_expected.saturating_sub(len_incomplete);
        let e = if len_from_input != 0 {
            // Get more bytes.
            buf[len_incomplete..len_expected].copy_from_slice(&bytes[..len_from_input]);
            // Check if the bytes became valid UTF-8 sequence.
            match str::from_utf8(&buf[..len_expected]) {
                Ok(_) => {
                    self.inner.extend(&buf[len_incomplete..len_expected]);
                    self.len_incomplete = 0;
                    return (&bytes[len_from_input..], true);
                }
                Err(e) => e,
            }
        } else {
            // Incomplete bytes might have some valid UTF-8 sequence.
            // Check if such prefix exists.
            match str::from_utf8(&buf[..len_expected]) {
                Ok(_) => {
                    self.len_incomplete = (len_incomplete - len_expected) as u8;
                    return (bytes, true);
                }
                Err(e) => e,
            }
        };

        // Cannot get valid UTF-8 sequence.
        // Remove leading bytes from incomplete bytes.
        assert_eq!(
            e.valid_up_to(),
            0,
            "[consistency] the buffer must have no valid leading characters here"
        );
        let error_len = match e.error_len() {
            Some(e) => e,
            None => {
                // Not enough input.
                assert_eq!(
                    len_from_input,
                    bytes.len(),
                    "[consistency] if more inputs are available, bytes must be \
                     able to be completed or discarded"
                );
                return (&bytes[len_from_input..], len_from_input != 0);
            }
        };
        self.inner.truncate(self.len_complete());
        self.inner.extend(REPLACEMENT_CHAR_STR.as_bytes());
        self.inner.extend(&buf[error_len..len_expected]);
        self.len_incomplete = (len_expected - error_len) as u8;

        (&bytes[len_from_input..], true)
    }

    /// Pops the first character in the buffer and returns it.
    ///
    /// Trailing incomplete character is ignored.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::from("hello");
    ///
    /// assert_eq!(queue.pop_char(), Some('h'));
    /// assert_eq!(queue.pop_char(), Some('e'));
    /// assert_eq!(queue.pop_char(), Some('l'));
    /// assert_eq!(queue.pop_char(), Some('l'));
    /// assert_eq!(queue.pop_char(), Some('o'));
    /// assert_eq!(queue.pop_char(), None);
    /// assert!(queue.is_empty());
    /// ```
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::from(b"a\xf0");
    ///
    /// assert_eq!(queue.pop_char(), Some('a'));
    /// assert_eq!(queue.pop_char(), None);
    /// assert!(!queue.is_empty());
    /// ```
    pub fn pop_char(&mut self) -> Option<char> {
        let (c, len) = self.first_char_and_len()?;
        self.inner.drain(..usize::from(len));

        Some(c)
    }

    /// Pops the first character from the queue and returns it.
    ///
    /// The trailing incomplete character is replaced with `U+FFFD REPLACEMENT
    /// CHARACTER`, if available.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let queue = StrQueue::from(b"abc\xce");
    /// let mut range = queue.chars_range(..);
    ///
    /// assert_eq!(range.pop_char_replaced(), Some('a'));
    /// assert_eq!(range.pop_char_replaced(), Some('b'));
    /// assert_eq!(range.pop_char_replaced(), Some('c'));
    /// assert_eq!(range.pop_char_replaced(), Some('\u{FFFD}'));
    /// assert_eq!(range.pop_char_replaced(), None);
    /// ```
    pub fn pop_char_replaced(&mut self) -> Option<char> {
        self.pop_char().or_else(|| {
            if self.is_empty() {
                return None;
            }
            self.inner.clear();
            Some('\u{FFFD}')
        })
    }

    /// Pops the first line in the queue and returns it.
    ///
    /// Incomplete lines are ignored.
    ///
    /// Note that it is unspecified whether the line will be removed from the queue,
    /// if the `PoppedLine` value is not dropped while the borrow it holds expires
    /// (e.g. due to [`core::mem::forget`]).
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// // Note that the last "world\r" is not considered as a complete line
    /// // since the line can be finally "world\r" or "world\r\n".
    /// let mut queue = StrQueue::from("Hello\nworld\r\nGoodbye\rworld\r");
    ///
    /// assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), Some("Hello\n"));
    /// assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), Some("world\r\n"));
    /// assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), Some("Goodbye\r"));
    /// assert_eq!(queue.pop_line().map(|v| v.to_string()).as_deref(), None);
    /// assert_eq!(queue.chars_range(..), "world\r");
    /// ```
    #[inline]
    pub fn pop_line(&mut self) -> Option<PoppedLine<'_>> {
        let line_len = self.first_line()?.len();
        let line_len = NonZeroUsize::new(line_len)
            .expect("[validity] a complete line must not be empty since it has a line break");
        Some(PoppedLine {
            queue: self,
            line_len,
        })
    }

    /// Pops the first [`Fragment`] from the queue and return it.
    ///
    /// In other words, takes as much content as possible from the queue.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::from(b"Hello \xce");
    /// let mut buf = String::new();
    ///
    /// while let Some(frag) = queue.pop_fragment() {
    ///     buf.push_str(&frag.to_string());
    /// }
    ///
    /// assert_eq!(buf, "Hello \u{FFFD}");
    /// assert!(queue.is_empty());
    /// ```
    pub fn pop_fragment(&mut self) -> Option<PoppedFragment<'_>> {
        if self.inner.is_empty() {
            return None;
        }

        let (former, latter) = self.inner.as_slices();
        if !former.is_empty() {
            let last_len = utf8::last_char_len_in_last_4bytes(former)
                .expect("[validity] the queue content must start with valid UTF-8 string");
            if last_len.is_complete() {
                return Some(PoppedFragment {
                    queue: self,
                    after_state: StateAfterPoppingFragment::Char { latter_consumed: 0 },
                });
            }
            if usize::from(last_len.available) != former.len() {
                // Non-empty complete string prefix exists.
                return Some(PoppedFragment {
                    queue: self,
                    after_state: StateAfterPoppingFragment::Former {
                        rest: last_len.available,
                    },
                });
            }

            let len_missing = last_len.len_missing();
            if last_len.len_missing() <= latter.len() {
                return Some(PoppedFragment {
                    queue: self,
                    after_state: StateAfterPoppingFragment::Char {
                        latter_consumed: len_missing as u8,
                    },
                });
            }

            return Some(PoppedFragment {
                queue: self,
                after_state: StateAfterPoppingFragment::AllConsumed,
            });
        }

        let last_len = utf8::last_char_len_in_last_4bytes(latter)
            .expect("[validity] the queue content must start with valid UTF-8 string");
        if last_len.is_complete() {
            return Some(PoppedFragment {
                queue: self,
                after_state: StateAfterPoppingFragment::Latter { rest: 0 },
            });
        }
        Some(PoppedFragment {
            queue: self,
            after_state: StateAfterPoppingFragment::AllConsumed,
        })
    }
}

/// Content access.
impl StrQueue {
    /// Returns the first character in the buffer and its length in bytes.
    #[must_use]
    fn first_char_and_len(&self) -> Option<(char, u8)> {
        let bytes = self.inner.iter().take(4).copied();
        utf8::take_char(bytes)
    }

    /// Returns the first character in the buffer.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let queue = StrQueue::from("hello");
    ///
    /// assert_eq!(queue.first_char(), Some('h'));
    /// ```
    #[inline]
    #[must_use]
    pub fn first_char(&self) -> Option<char> {
        self.first_char_and_len().map(|(c, _len)| c)
    }

    /// Returns the first complete line in the buffer.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    ///
    /// let mut queue = StrQueue::from("hello");
    /// // No complete line.
    /// assert_eq!(queue.first_line(), None);
    ///
    /// queue.push_bytes(b"\r");
    /// // Still no complete line: "hello\r" is considered incomplete since
    /// // it cannot be decided whether the line is "hello\r" or "hello\r\n".
    /// assert_eq!(queue.first_line(), None);
    ///
    /// queue.push_bytes(b"\n");
    /// assert_eq!(
    ///     queue.first_line().map(|line| line.to_string()).as_deref(),
    ///     Some("hello\r\n")
    /// );
    /// ```
    #[inline]
    #[must_use]
    pub fn first_line(&self) -> Option<CharsRange<'_>> {
        self.chars_range(..).first_line()
    }
}

/// Iterators.
impl StrQueue {
    /// Turns the queue into an iterator of the characters.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let queue = StrQueue::from("alpha \u{03B1} beta \u{03B2}");
    ///
    /// assert_eq!(
    ///     queue.into_chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha \u{03B1} beta \u{03B2}"
    /// );
    /// ```
    ///
    /// Trailing possibly invalid bytes (i.e. incomplete characters) can be
    /// replaced with `U+FFFD REPLACEMENT CHARACTER`, or be simply ignored.
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from(b"alpha->\xce");
    ///
    /// // The trailing incomplete character is ignored.
    /// assert_eq!(
    ///     queue.clone().into_chars(PartialHandling::Ignore).collect::<String>(),
    ///     "alpha->"
    /// );
    /// // The trailing incomplete character is replaced with U+FFFD.
    /// assert_eq!(
    ///     queue.into_chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha->\u{FFFD}"
    /// );
    /// ```
    #[inline]
    #[must_use]
    pub fn into_chars(self, partial_handling: PartialHandling) -> IntoChars {
        IntoChars::new(self, partial_handling)
    }

    /// Turns the queue into an iterator of the characters.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let queue = StrQueue::from("alpha \u{03B1} beta \u{03B2}");
    ///
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha \u{03B1} beta \u{03B2}"
    /// );
    /// ```
    ///
    /// Trailing possibly invalid bytes (i.e. incomplete characters) can be
    /// replaced with `U+FFFD REPLACEMENT CHARACTER`, or be simply ignored.
    ///
    /// ```
    /// use str_queue::{PartialHandling, StrQueue};
    ///
    /// let mut queue = StrQueue::from(b"alpha->\xce");
    /// // The trailing incomplete character is ignored.
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Ignore).collect::<String>(),
    ///     "alpha->"
    /// );
    /// // The trailing incomplete character is replaced with U+FFFD.
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha->\u{FFFD}"
    /// );
    ///
    /// queue.push_bytes(b"\xb1");
    /// assert_eq!(
    ///     queue.chars(PartialHandling::Emit).collect::<String>(),
    ///     "alpha->\u{03B1}"
    /// );
    /// ```
    #[inline]
    #[must_use]
    pub fn chars(&self, partial_handling: PartialHandling) -> Chars<'_> {
        Chars::new(self, .., partial_handling)
    }

    /// Returns the chars iterator for the range.
    ///
    /// # Panics
    ///
    /// Panics if the start index of the range does not lie on UTF-8 sequence boundary.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{Fragment, PartialHandling, StrQueue};
    ///
    /// let queue = StrQueue::from("alpha\u{03B1}");
    ///
    /// assert_eq!(
    ///     queue.range_chars(3..6, PartialHandling::Emit).collect::<String>(),
    ///     "ha\u{FFFD}"
    /// );
    /// assert_eq!(
    ///     queue.range_chars(3..6, PartialHandling::Ignore).collect::<String>(),
    ///     "ha"
    /// );
    /// ```
    #[must_use]
    pub fn range_chars<R>(&self, range: R, partial_handling: PartialHandling) -> Chars<'_>
    where
        R: RangeBounds<usize>,
    {
        Chars::new(self, range, partial_handling)
    }

    /// Returns an iterator of content fragments.
    ///
    /// # Examples
    ///
    /// The code below are redundant since they are intended to show how to use [`Fragment`].
    /// To simply create a string from the queue, use [`StrQueue::display`] method.
    ///
    /// ```
    /// use str_queue::{Fragment, PartialHandling, StrQueue};
    ///
    /// // Note that `\xce` can appear as a first byte of valid UTF-8 sequence.
    /// let queue = StrQueue::from(b"hello \xce");
    ///
    /// let mut buf = String::new();
    /// for frag in queue.fragments(PartialHandling::Emit) {
    ///     match frag {
    ///         Fragment::Str(s) => buf.push_str(s),
    ///         Fragment::Char(c) => buf.push(c),
    ///         Fragment::Incomplete => buf.push_str("<<incomplete>>"),
    ///     }
    /// }
    /// assert_eq!(buf, "hello <<incomplete>>");
    /// ```
    ///
    /// When `PartialHandling::Ignore` is passed, `Fragment::Incomplete` won't
    /// be emitted even if the queue contains trailing incomplete character.
    ///
    /// ```
    /// use str_queue::{Fragment, PartialHandling, StrQueue};
    ///
    /// // Note that `\xce` can appear as a first byte of valid UTF-8 sequence.
    /// let queue = StrQueue::from(b"hello \xce");
    ///
    /// let mut buf = String::new();
    /// for frag in queue.fragments(PartialHandling::Ignore) {
    ///     match frag {
    ///         Fragment::Str(s) => buf.push_str(s),
    ///         Fragment::Char(c) => buf.push(c),
    ///         Fragment::Incomplete => unreachable!("`PartialHandling::Ignore` is specified"),
    ///     }
    /// }
    /// assert_eq!(buf, "hello ");
    /// ```
    ///
    /// Note that `StrQueue` will immediately replace non-incomplete invalid
    /// bytes (such as `\xff` and `\xce\xff`).
    /// Such already-replaced bytes will be printed as `U+FFFD` even if
    /// `PartialHandling::Ignore` is specified.
    ///
    /// ```
    /// use str_queue::{Fragment, PartialHandling, StrQueue};
    ///
    /// // Note that valid UTF-8 sequences can start with `\xf0`, but
    /// // never start with `\xf0\xf0`.
    /// // So, the first `\xf0` is immediately replaced with U+FFFD, and the
    /// // second `\xf0` is considered as a prefix of an incomplete character.
    /// let queue = StrQueue::from(b"hello \xf0\xf0");
    ///
    /// let mut buf = String::new();
    /// for frag in queue.fragments(PartialHandling::Emit) {
    ///     match frag {
    ///         Fragment::Str(s) => buf.push_str(s),
    ///         Fragment::Char(c) => buf.push(c),
    ///         Fragment::Incomplete => buf.push_str("<<incomplete>>"),
    ///     }
    /// }
    /// assert_eq!(buf, "hello \u{FFFD}<<incomplete>>");
    /// ```
    ///
    /// ```
    /// use str_queue::{Fragment, PartialHandling, StrQueue};
    ///
    /// // Note that valid UTF-8 sequences can start with `\xf0`, but
    /// // never start with `\xf0\xf0`.
    /// // So, the first `\xf0` is immediately replaced with U+FFFD, and the
    /// // second `\xf0` is considered as a prefix of an incomplete character.
    /// let queue = StrQueue::from(b"hello \xf0\xf0");
    ///
    /// let mut buf = String::new();
    /// for frag in queue.fragments(PartialHandling::Ignore) {
    ///     match frag {
    ///         Fragment::Str(s) => buf.push_str(s),
    ///         Fragment::Char(c) => buf.push(c),
    ///         Fragment::Incomplete => unreachable!("`PartialHandling::Ignore` is specified"),
    ///     }
    /// }
    /// // Note that the first `\xf0` is replaced with `U+FFFD`.
    /// assert_eq!(buf, "hello \u{FFFD}");
    /// ```
    #[inline]
    #[must_use]
    pub fn fragments(&self, partial_handling: PartialHandling) -> Fragments<'_> {
        self.range_fragments(.., partial_handling)
    }

    /// Returns the fragments iterator for the range.
    ///
    /// # Panics
    ///
    /// Panics if the start index of the range does not lie on UTF-8 sequence boundary.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::{Fragment, PartialHandling, StrQueue};
    ///
    /// let queue = StrQueue::from("alpha\u{03B1}");
    ///
    /// assert_eq!(
    ///     queue.range_fragments(3..6, PartialHandling::Emit).to_string(),
    ///     "ha\u{FFFD}"
    /// );
    /// assert_eq!(
    ///     queue.range_fragments(3..6, PartialHandling::Ignore).to_string(),
    ///     "ha"
    /// );
    /// ```
    #[inline]
    #[must_use]
    pub fn range_fragments<R>(&self, range: R, partial_handling: PartialHandling) -> Fragments<'_>
    where
        R: RangeBounds<usize>,
    {
        Fragments::new(self, range, partial_handling)
    }

    /// Returns an object that implements [`Display`] for printing the content.
    ///
    /// # Examples
    ///
    /// ```
    /// use str_queue::StrQueue;
    /// use str_queue::PartialHandling::{Emit, Ignore};
    ///
    /// let mut queue = StrQueue::from(b"alpha->\xce");
    /// assert_eq!(queue.display(Ignore).to_string(), "alpha->");
    /// assert_eq!(queue.display(Emit).to_string(), "alpha->\u{FFFD}");
    ///
    /// queue.push_bytes(b"\xb1");
    /// assert_eq!(queue.display(Ignore).to_string(), "alpha->\u{03B1}");
    /// assert_eq!(queue.display(Emit).to_string(), "alpha->\u{03B1}");
    /// ```
    ///
    /// [`Display`]: `fmt::Display`
    #[inline]
    #[must_use]
    pub fn display(&self, partial_handling: PartialHandling) -> Display<'_> {
        Display::new(self, partial_handling)
    }
}

impl From<&str> for StrQueue {
    #[inline]
    fn from(s: &str) -> Self {
        let mut this = Self::with_capacity(s.len());
        this.push_str(s);
        this
    }
}

impl From<&[u8]> for StrQueue {
    #[inline]
    fn from(s: &[u8]) -> Self {
        let mut this = Self::with_capacity(s.len());
        this.push_bytes(s);
        this
    }
}

impl<const N: usize> From<&[u8; N]> for StrQueue {
    #[inline]
    fn from(s: &[u8; N]) -> Self {
        Self::from(&s[..])
    }
}

impl hash::Hash for StrQueue {
    fn hash<H: hash::Hasher>(&self, state: &mut H) {
        self.inner.hash(state);
    }
}

/// Handling of suffix bytes of a partial (incomplete) character.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PartialHandling {
    /// Suffix bytes for an incomplete character will be ignored.
    Ignore,
    /// Suffix bytes for an incomplete character will be visible as
    /// `U+FFFD REPLACEMENT CHARACTER`.
    Emit,
}

impl PartialHandling {
    /// Returns true if `self` is `Emit`.
    #[inline]
    #[must_use]
    fn is_emit(self) -> bool {
        self == Self::Emit
    }

    /// Returns true if `self` is `Ignore`.
    #[inline]
    #[must_use]
    fn is_ignore(self) -> bool {
        self == Self::Ignore
    }
}

/// Helper struct for printing [`StrQueue`].
///
/// Created by [`StrQueue::display`] method.
#[derive(Debug)]
pub struct Display<'a> {
    /// Queue.
    queue: &'a StrQueue,
    /// Whether an incomplete character (replaced with U+FFFD) should be emitted.
    partial_handling: PartialHandling,
}

impl<'a> Display<'a> {
    /// Creates a new `Display`.
    #[inline]
    #[must_use]
    fn new(queue: &'a StrQueue, partial_handling: PartialHandling) -> Self {
        Self {
            queue,
            partial_handling,
        }
    }
}

impl fmt::Display for Display<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.queue
            .fragments(self.partial_handling)
            .try_for_each(|frag| frag.fmt(f))
    }
}

/// A line popped from [`StrQueue`] using [`pop_line`][`StrQueue::pop_line`] method.
///
/// Note that it is unspecified whether the line will be removed from the queue,
/// if the `PoppedLine` value is not dropped while the borrow it holds expires
/// (e.g. due to [`core::mem::forget`]).
#[derive(Debug)]
pub struct PoppedLine<'a> {
    /// Queue.
    queue: &'a mut StrQueue,
    /// Line length.
    ///
    /// A complete line cannot be empty since it has a line break.
    line_len: NonZeroUsize,
}

impl<'a> PoppedLine<'a> {
    /// Returns the chars range for the line.
    #[inline]
    #[must_use]
    pub fn to_chars_range(&self) -> CharsRange<'_> {
        self.queue.chars_range(..self.line_len.get())
    }

    /// Returns the bytes range for the line.
    #[inline]
    #[must_use]
    pub fn to_bytes_range(&self) -> BytesRange<'_> {
        self.queue.bytes_range(..self.line_len.get())
    }

    /// Returns the length of the line.
    #[inline]
    #[must_use]
    pub fn len(&self) -> usize {
        self.line_len.get()
    }

    /// Returns whether the line is empty, i.e. **always returns false**.
    ///
    /// A complete line to be removed cannot be empty since it has a line break.
    #[inline]
    #[must_use]
    pub const fn is_empty(&self) -> bool {
        false
    }
}

impl fmt::Display for PoppedLine<'_> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.to_chars_range().fmt(f)
    }
}

impl core::ops::Drop for PoppedLine<'_> {
    #[inline]
    fn drop(&mut self) {
        self.queue.inner.drain(..self.line_len.get());
    }
}

impl PartialEq for PoppedLine<'_> {
    #[inline]
    fn eq(&self, other: &PoppedLine<'_>) -> bool {
        self.to_bytes_range().eq(&other.to_bytes_range())
    }
}

impl Eq for PoppedLine<'_> {}

impl PartialOrd for PoppedLine<'_> {
    #[inline]
    fn partial_cmp(&self, rhs: &PoppedLine<'_>) -> Option<Ordering> {
        self.to_bytes_range().partial_cmp(&rhs.to_bytes_range())
    }
}

impl Ord for PoppedLine<'_> {
    #[inline]
    fn cmp(&self, rhs: &PoppedLine<'_>) -> Ordering {
        self.to_bytes_range().cmp(&rhs.to_bytes_range())
    }
}

impl PartialEq<str> for PoppedLine<'_> {
    #[inline]
    fn eq(&self, other: &str) -> bool {
        self.to_bytes_range().eq(other.as_bytes())
    }
}

impl PartialOrd<str> for PoppedLine<'_> {
    #[inline]
    fn partial_cmp(&self, rhs: &str) -> Option<Ordering> {
        self.to_bytes_range().partial_cmp(rhs.as_bytes())
    }
}

impl PartialEq<PoppedLine<'_>> for str {
    #[inline]
    fn eq(&self, other: &PoppedLine<'_>) -> bool {
        other.to_bytes_range().eq(self.as_bytes())
    }
}

impl PartialOrd<PoppedLine<'_>> for str {
    #[inline]
    fn partial_cmp(&self, rhs: &PoppedLine<'_>) -> Option<Ordering> {
        rhs.to_bytes_range()
            .partial_cmp(self.as_bytes())
            .map(Ordering::reverse)
    }
}

impl PartialEq<&str> for PoppedLine<'_> {
    #[inline]
    fn eq(&self, other: &&str) -> bool {
        self.eq(*other)
    }
}

impl PartialOrd<&str> for PoppedLine<'_> {
    #[inline]
    fn partial_cmp(&self, rhs: &&str) -> Option<Ordering> {
        self.partial_cmp(*rhs)
    }
}

impl PartialEq<PoppedLine<'_>> for &str {
    #[inline]
    fn eq(&self, other: &PoppedLine<'_>) -> bool {
        other.eq(*self)
    }
}

impl PartialOrd<PoppedLine<'_>> for &str {
    #[inline]
    fn partial_cmp(&self, rhs: &PoppedLine<'_>) -> Option<Ordering> {
        rhs.partial_cmp(*self).map(Ordering::reverse)
    }
}

/// A state after popping a fragment from a queue.
#[derive(Debug, Clone, Copy)]
enum StateAfterPoppingFragment {
    /// The former buffer is partially consumed.
    Former {
        /// The unused size in the former buffer at the tail.
        ///
        /// This must be less than 4 since a complete character is at most
        /// 4 bytes in UTF-8.
        rest: u8,
    },
    /// The former is completely consumed and the latter is partially consumed.
    Char {
        /// The consumed size in the latter buffer at the head.
        ///
        /// This must be less than 4 since a complete character is at most
        /// 4 bytes in UTF-8.
        latter_consumed: u8,
    },
    /// The latter buffer is partially consumed.
    Latter {
        /// The unused size in the latter buffer at the tail.
        ///
        /// This must be less than 4 since a complete character is at most
        /// 4 bytes in UTF-8.
        rest: u8,
    },
    /// All bytes are consumed.
    ///
    /// This variant indicates that the buffer is currently not yet empty since
    /// the buffer has a trailing incomplete character.
    AllConsumed,
}

/// A fragment popped from [`StrQueue`] using [`pop_fragment`][`StrQueue::pop_fragment`] method.
///
/// Note that it is unspecified whether the line will be removed from the queue,
/// if the `PoppedFragment` value is not dropped while the borrow it holds expires
/// (e.g. due to [`core::mem::forget`]).
#[derive(Debug)]
pub struct PoppedFragment<'a> {
    /// Queue.
    queue: &'a mut StrQueue,
    /// State after popping fragment.
    after_state: StateAfterPoppingFragment,
}

impl<'a> PoppedFragment<'a> {
    /// Returns the fragment for the line.
    #[inline]
    #[must_use]
    pub fn to_fragment(&self) -> Fragment<'_> {
        use StateAfterPoppingFragment::*;

        match self.after_state {
            Former { rest } => {
                let (former, _) = self.queue.inner.as_slices();
                let len = former.len() - usize::from(rest);
                let s = str::from_utf8(&former[..len])
                    .expect("[consistency] the range must be valid UTF-8 string");
                Fragment::Str(s)
            }
            Char { latter_consumed } => {
                let (former, latter) = self.queue.inner.as_slices();
                let (c, len) = utf8::take_char(
                    former
                        .iter()
                        .chain(&latter[..usize::from(latter_consumed)])
                        .copied(),
                )
                .expect(
                    "[consistency] it should be already validated \
                     that a complete character is available",
                );
                debug_assert_eq!(
                    usize::from(len),
                    former.len() + usize::from(latter_consumed),
                    "[consistency] a character lay on the specified area in the buffers"
                );
                Fragment::Char(c)
            }
            Latter { rest } => {
                let (_, latter) = self.queue.inner.as_slices();
                let len = latter.len() - usize::from(rest);
                let s = str::from_utf8(&latter[..len])
                    .expect("[consistency] the range must be valid UTF-8 string");
                Fragment::Str(s)
            }
            AllConsumed => Fragment::Incomplete,
        }
    }

    /// Returns the chars range for the line.
    #[inline]
    #[must_use]
    pub fn to_chars_range(&self) -> CharsRange<'_> {
        use StateAfterPoppingFragment::*;

        match self.after_state {
            Former { rest } => {
                let (former, _) = self.queue.inner.as_slices();
                let len = former.len() - usize::from(rest);
                self.queue.chars_range(..len)
            }
            Char { latter_consumed } => {
                let (former, _) = self.queue.inner.as_slices();
                debug_assert!(
                    former.len() < 4,
                    "[consistency] the former buffer must not have a complete character"
                );
                let end = former.len() + usize::from(latter_consumed);
                self.queue.chars_range(..end)
            }
            Latter { rest } => {
                let (former, latter) = self.queue.inner.as_slices();
                debug_assert!(
                    former.is_empty(),
                    "[consistency] the former buffer must have been completely consumed"
                );
                let len = latter.len() - usize::from(rest);
                self.queue.chars_range(..len)
            }
            AllConsumed => self.queue.chars_range(..),
        }
    }

    /// Returns the bytes range for the line.
    #[inline]
    #[must_use]
    pub fn to_bytes_range(&self) -> BytesRange<'_> {
        self.to_chars_range().into()
    }

    /// Returns the length of the line.
    #[inline]
    #[must_use]
    pub fn len(&self) -> usize {
        use StateAfterPoppingFragment::*;

        match self.after_state {
            Former { rest } => {
                let (former, _) = self.queue.inner.as_slices();
                former.len() - usize::from(rest)
            }
            Char { latter_consumed } => {
                let (former, _) = self.queue.inner.as_slices();
                debug_assert!(
                    former.len() < 4,
                    "[consistency] the former buffer must not have a complete character"
                );
                former.len() + usize::from(latter_consumed)
            }
            Latter { rest } => {
                let (former, latter) = self.queue.inner.as_slices();
                debug_assert!(
                    former.is_empty(),
                    "[consistency] the former buffer must have been completely consumed"
                );
                latter.len() - usize::from(rest)
            }
            AllConsumed => self.queue.len(),
        }
    }

    /// Returns whether the line is empty, i.e. **always returns false**.
    ///
    /// A complete line to be removed cannot be empty since it has a line break.
    #[inline]
    #[must_use]
    pub const fn is_empty(&self) -> bool {
        false
    }
}

impl fmt::Display for PoppedFragment<'_> {
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.to_fragment().fmt(f)
    }
}

impl core::ops::Drop for PoppedFragment<'_> {
    #[inline]
    fn drop(&mut self) {
        self.queue.inner.drain(..self.len());
    }
}

impl PartialEq for PoppedFragment<'_> {
    #[inline]
    fn eq(&self, other: &PoppedFragment<'_>) -> bool {
        self.to_bytes_range().eq(&other.to_bytes_range())
    }
}

impl Eq for PoppedFragment<'_> {}

impl PartialOrd for PoppedFragment<'_> {
    #[inline]
    fn partial_cmp(&self, rhs: &PoppedFragment<'_>) -> Option<Ordering> {
        self.to_bytes_range().partial_cmp(&rhs.to_bytes_range())
    }
}

impl Ord for PoppedFragment<'_> {
    #[inline]
    fn cmp(&self, rhs: &PoppedFragment<'_>) -> Ordering {
        self.to_bytes_range().cmp(&rhs.to_bytes_range())
    }
}

impl PartialEq<str> for PoppedFragment<'_> {
    #[inline]
    fn eq(&self, other: &str) -> bool {
        self.to_bytes_range().eq(other.as_bytes())
    }
}

impl PartialOrd<str> for PoppedFragment<'_> {
    #[inline]
    fn partial_cmp(&self, rhs: &str) -> Option<Ordering> {
        self.to_bytes_range().partial_cmp(rhs.as_bytes())
    }
}

impl PartialEq<PoppedFragment<'_>> for str {
    #[inline]
    fn eq(&self, other: &PoppedFragment<'_>) -> bool {
        other.to_bytes_range().eq(self.as_bytes())
    }
}

impl PartialOrd<PoppedFragment<'_>> for str {
    #[inline]
    fn partial_cmp(&self, rhs: &PoppedFragment<'_>) -> Option<Ordering> {
        rhs.to_bytes_range()
            .partial_cmp(self.as_bytes())
            .map(Ordering::reverse)
    }
}

impl PartialEq<&str> for PoppedFragment<'_> {
    #[inline]
    fn eq(&self, other: &&str) -> bool {
        self.eq(*other)
    }
}

impl PartialOrd<&str> for PoppedFragment<'_> {
    #[inline]
    fn partial_cmp(&self, rhs: &&str) -> Option<Ordering> {
        self.partial_cmp(*rhs)
    }
}

impl PartialEq<PoppedFragment<'_>> for &str {
    #[inline]
    fn eq(&self, other: &PoppedFragment<'_>) -> bool {
        other.eq(*self)
    }
}

impl PartialOrd<PoppedFragment<'_>> for &str {
    #[inline]
    fn partial_cmp(&self, rhs: &PoppedFragment<'_>) -> Option<Ordering> {
        rhs.partial_cmp(*self).map(Ordering::reverse)
    }
}
