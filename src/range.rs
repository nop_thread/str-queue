//! Range in a queue.

mod bytes;
mod chars;

pub use self::bytes::BytesRange;
pub use self::chars::{CharsRange, CharsRangePoppedLine};
