# str-queue

[![Build Status](https://gitlab.com/nop_thread/str-queue/badges/develop/pipeline.svg)](https://gitlab.com/nop_thread/str-queue/pipelines/)
[![Latest version](https://img.shields.io/crates/v/str-queue.svg)](https://crates.io/crates/str-queue)
[![Documentation](https://docs.rs/str-queue/badge.svg)](https://docs.rs/str-queue)
![Minimum supported rustc version: 1.55](https://img.shields.io/badge/rustc-1.55+-lightgray.svg)

A queue for a string.

## Feature flags

* `std`
    + Enabled by default.
    + Use `std`-only items, such as `std::error::Error` trait.
        - Currently this feature has no effects.
    + Even when this is disabled, `alloc` crate is still necessary.
* `memchr`
    + Use [`memchr`] crate for faster search.
* `std_with_memchr`
    + Enables `std` and `memchr` feature of this crate, and `std` feature of `memchr` crate.

[`memchr`]: https://crates.io/crates/memchr

## License

Licensed under either of

* Apache License, Version 2.0, ([LICENSE-APACHE.txt](LICENSE-APACHE.txt) or
  <https://www.apache.org/licenses/LICENSE-2.0>)
* MIT license ([LICENSE-MIT.txt](LICENSE-MIT.txt) or
  <https://opensource.org/licenses/MIT>)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.
