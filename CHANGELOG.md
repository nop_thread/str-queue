# Change Log

## [Unreleased]

### Breaking changes

* Bump MSRV to 1.55.

## [0.0.1]

Initial release.

[Unreleased]: <https://gitlab.com/nop_thread/str-queue/-/compare/v0.0.1...develop>
[0.0.1]: <https://gitlab.com/nop_thread/str-queue/-/releases/v0.0.1>
